﻿using UnityEngine;
using System.Collections;

public class LaunchSceneController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine ("startGame");
	}

	IEnumerator startGame() {
		float fadeTime = GameObject.FindWithTag ("LaunchScene").GetComponent<Fader>().BeginFade (-1);
		yield return new WaitForSeconds(fadeTime + 5f);
		fadeTime = GameObject.FindWithTag ("LaunchScene").GetComponent<Fader>().BeginFade (1);
		yield return new WaitForSeconds(fadeTime);
		Application.LoadLevel ("GameStartScene");
	}

	void OnApplicationQuit() {
		PlayerPrefs.SetInt ("Show Ad", 1);
	}
}

﻿using UnityEngine;
using System.Collections;

public class ItemController : MonoBehaviour {
	float timeOnField;
	// Use this for initialization
	void Start () {
		timeOnField = 0;
	}
	
	// Update is called once per frame
	void Update () {
		timeOnField += Time.deltaTime;
		checkFieldStatus ();
	}

	void checkFieldStatus() {
		if (timeOnField > 8f) {
			Destroy (gameObject);
		}
	}
}

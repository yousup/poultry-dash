﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class GameOverController : MonoBehaviour {
	public GUIText scoreText;
	public GUIText highScoreText;
	public GUISkin skin;
	private int showAd;
	// Use this for initialization
	void Start () {
		showAd = PlayerPrefs.GetInt ("Show Ad", defaultValue: 1);
		int score = GameController.score;
		scoreText.text += score;
		if (score > PlayerPrefs.GetInt ("High Score")) {
			PlayerPrefs.SetInt ("High Score", score);
			highScoreText.text += score;
			scoreText.text += " <color=red>NEW HIGH!</color>";
		} else {
			highScoreText.text += PlayerPrefs.GetInt ("High Score");
		}
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.fontSize = Screen.width / 14;
		highScoreText.fontSize = Screen.width / 14;
	}

	void OnGUI () {
		GUI.skin = skin;
		float width = Screen.width/5.7f;
		float height = Screen.height/8;
		skin.button.fontSize = Screen.width/20;
		if (GUI.Button (new Rect (Screen.width / 2 - width/2, Screen.height - height - 70, width, height), "Play Again")) {
			StartCoroutine (ShowAdWhenReady ());
			Application.LoadLevel ("GameStartScene");
		}
	}

	IEnumerator ShowAdWhenReady()
	{	
		while (!Advertisement.IsReady ())
			yield return null;
		if (showAd % 2 == 0) {
			Advertisement.Show ();

		}
		PlayerPrefs.SetInt ("Show Ad", showAd + 1);
	}

	void OnApplicationQuit() {
		PlayerPrefs.SetInt ("Show Ad", 1);
	}
}

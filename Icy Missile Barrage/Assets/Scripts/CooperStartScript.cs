﻿using UnityEngine;
using System.Collections;

public class CooperStartScript : MonoBehaviour {
	private float timeSincePlop = 0;
	public bool inGracePeriod = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (inGracePeriod) {
			timeSincePlop += Time.deltaTime;
			if (timeSincePlop > 3f) {
				inGracePeriod = false;
			}
		} else {
			if (!gameObject.GetComponent<Renderer> ().isVisible) {
				Destroy (gameObject);
			}
		}
	}
}

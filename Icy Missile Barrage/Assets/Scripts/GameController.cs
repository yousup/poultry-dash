﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class GameController : MonoBehaviour {
	public GUIText scoreText;
	static public int score;
	public GUISkin buttonSkin;
	public GUISkin bombSkin;
//	private float timeSinceStart = 0f;
	// Use this for initialization
	void Start () {
//		Advertisement.Initialize ("1003583");
		score = 0;
		updateScore ();
		GameObject.FindWithTag ("InstructionText").GetComponent<GUIText> ().fontSize = Screen.width/17;
	}

	public void startGame() {
		Invoke ("spawnMissile", .2f);
		StartCoroutine(Fade(.6f, 0f));
		GameObject.FindWithTag ("ObjectFactory").GetComponent<ObjectSpawner> ().invokeSpawnItem ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnGUI() {
		GUI.skin = bombSkin;
		float width = Screen.width / 6;
		float height = Screen.height / 8.5f;
		GameObject chicken = GameObject.FindGameObjectWithTag ("Cooper");
		GUI.Box (new Rect(Screen.width - width+6, 0, width-6, height), "");

		if (chicken != null && chicken.GetComponent<CooperController> ().numberOfBombs >= 1) {
			if (GUI.Button (new Rect (Screen.width - width/3+3, 8, width/3-6, height-16), "")) {
				chicken.GetComponent<CooperController>().numberOfBombs--;
				GameObject.FindWithTag ("ObjectFactory").GetComponent<ObjectSpawner> ().bombPressed();
			}
		}
		if (chicken != null && chicken.GetComponent<CooperController> ().numberOfBombs >= 2) {
			if (GUI.Button (new Rect (Screen.width - 2*width/3+6, 8, width/3-6, height-16), "")) {
				chicken.GetComponent<CooperController>().numberOfBombs--;
				GameObject.FindWithTag ("ObjectFactory").GetComponent<ObjectSpawner> ().bombPressed();
			}
		}
		if (chicken != null && chicken.GetComponent<CooperController> ().numberOfBombs >= 3) {
			if (GUI.Button (new Rect (Screen.width - width+9, 8, width/3-6, height-16), "")) {
				chicken.GetComponent<CooperController>().numberOfBombs--;
				GameObject.FindWithTag ("ObjectFactory").GetComponent<ObjectSpawner> ().bombPressed();
			}
		}
	}

	void updateScore() {
		scoreText.text = score.ToString(); 
	}

	void spawnMissile() {
		GameObject.FindWithTag ("ObjectFactory").GetComponent<ObjectSpawner> ().SpawnMissile ();
	}

	public void addScore(int newScoreValue) {
		score += newScoreValue;
		updateScore ();
	}

	IEnumerator Fade(float time, float targetAlpha) {
		yield return new WaitForSeconds(3);
		float t = 0.0f;
		Color current = GameObject.FindWithTag ("InstructionText").GetComponent<GUIText> ().material.color;
		float currentAlpha = current.a;
		while (t <= 1f) {
			current.a = Mathf.Lerp(currentAlpha, targetAlpha, t);
			GameObject.FindWithTag ("InstructionText").GetComponent<GUIText> ().material.color = current;
			t += Time.deltaTime / time;
			yield return new WaitForSeconds(0);
		}
		current.a = targetAlpha;
		GameObject.FindWithTag ("InstructionText").GetComponent<GUIText> ().material.color = current;
	}
	void OnApplicationQuit() {
		PlayerPrefs.SetInt ("Show Ad", 1);
	}
}
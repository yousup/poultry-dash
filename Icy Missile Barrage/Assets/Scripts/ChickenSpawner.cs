﻿using UnityEngine;
using System.Collections;

public class ChickenSpawner : MonoBehaviour {
	public GameObject chicken;
	private bool gameHasStarted = false;
	public GUISkin buttonSkin;
	private Transform spawnPointRight;
	// Use this for initialization
	void Start () {
		spawnPointRight = GameObject.Find ("SpawnRight").transform;
		spawnChicken ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {
		if (!gameHasStarted) {
			GUI.skin = buttonSkin;
			buttonSkin.button.fontSize = Screen.width/18;
			float width = Screen.width/5;
			float height = Screen.height/7;
			if (GUI.Button (new Rect (Screen.width / 2 - width/2, 2*Screen.height/3, width, height), "Play!")) {
				//				GameObject.FindGameObjectWithTag ("Cooper").GetComponent<SpriteRenderer>().enabled = true;
//				GameObject.FindGameObjectWithTag ("Cooper").GetComponent<CooperController>().enabled = true;
				Application.LoadLevel ("GameScene");
				gameHasStarted = true;
			}
		}
	}

	public void spawnChicken() {
		Camera camera = Camera.main;
		float yMax = camera.orthographicSize - 0.5f;
		Vector3 chickenPoint;
		chickenPoint = new Vector3 (spawnPointRight.position.x, Random.Range (-yMax, yMax), 
		             chicken.transform.position.z);
		GameObject clone = Instantiate (chicken, chickenPoint, Quaternion.identity) as GameObject;
		clone.GetComponent<Rigidbody2D>().velocity = gameObject.transform.rotation * new Vector2(-2f, 0);
		Invoke ("spawnChicken", .83f);
	}

	void OnApplicationQuit() {
		PlayerPrefs.SetInt ("Show Ad", 1);
	}
}

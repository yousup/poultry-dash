﻿using UnityEngine;
using System.Collections;

public class ObjectSpawner : MonoBehaviour {
	public float missileMinSpawnTime; 
	public float missileMaxSpawnTime;
	public float itemMinSpawnTime;
	public float itemMaxSpawnTime;
//	public GameObject missilePrefab;
	public float spawnAngle;
	public GameObject[] items = new GameObject[3];

	private Transform spawnPointRight;
	private Transform spawnPointLeft;
	private Transform spawnPointTop;
	private Transform spawnPointBottom;
	private Transform[] spawnPoints = new Transform[4];
	// Use this for initialization
	void Start () {
		spawnPointRight = GameObject.Find ("SpawnPoint Right").transform; spawnPoints [0] = spawnPointRight;
		spawnPointLeft = GameObject.Find ("SpawnPoint Left").transform; spawnPoints [1] = spawnPointLeft;
		spawnPointTop = GameObject.Find ("SpawnPoint Top").transform; spawnPoints [2] = spawnPointTop;
		spawnPointBottom = GameObject.Find ("SpawnPoint Bottom").transform; spawnPoints [3] = spawnPointBottom;
//		Invoke("SpawnMissile",minSpawnTime);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SpawnMissile()
	{
//		Debug.Log("TODO: Spawn a missile at " + Time.timeSinceLevelLoad);
		GameObject missilePrefab = ObjectPoolerScript.current.getPooledObject ();
		missilePrefab.GetComponent<Animator> ().SetBool ("isBombPressed", false);
		if (missilePrefab == null) {
			return;
		}
		Transform spawnPoint = spawnPoints[Random.Range(0,4)];
		Camera camera = Camera.main;
//		Vector3 cameraPos = camera.transform.position;
		float xMax = camera.aspect * camera.orthographicSize;
//		float xRange = camera.aspect * camera.orthographicSize * 1.75f;
		float yMax = camera.orthographicSize - 0.5f;
		Vector3 missilePos;

		if (spawnPoint == spawnPointRight || spawnPoint == spawnPointLeft) {
			missilePos = new Vector3 (spawnPoint.position.x, Random.Range (-yMax, yMax), 
			                          missilePrefab.transform.position.z);
		} else {
			missilePos = new Vector3 (Random.Range (-xMax, xMax), spawnPoint.position.y, 
			                          missilePrefab.transform.position.z);
		}

		Vector3 chicken = GameObject.FindWithTag ("Cooper").transform.position;
		Vector3 target = missilePos - chicken;
		float angle = Mathf.Atan2 (-target.y, -target.x) * Mathf.Rad2Deg - 90f;
		missilePrefab.transform.position = missilePos;
		missilePrefab.transform.rotation = Quaternion.Euler (0, 0, angle);
		missilePrefab.SetActive (true);
		Invoke("SpawnMissile", Random.Range(missileMinSpawnTime, missileMaxSpawnTime));
	}

	public void SpawnItem() {
		GameObject item = items[Random.Range(0, items.Length)];
		Camera camera = Camera.main;
		Vector3 cameraPos = camera.transform.position;
		float xMax = camera.aspect * camera.orthographicSize;
		float yMax = camera.orthographicSize - 0.5f;
		Vector3 itemPos = new Vector3(cameraPos.x + Random.Range(-xMax, xMax),
		                   Random.Range(-yMax, yMax),
		                   item.transform.position.z);
		Instantiate (item, itemPos, Quaternion.identity);
		Invoke ("SpawnItem", Random.Range (itemMinSpawnTime, itemMaxSpawnTime));
	}

	public void invokeSpawnItem() {
		Invoke ("SpawnItem", Random.Range (4, 5));
	}

	public void setAllInactive() {
		ObjectPoolerScript.current.setAllInactive ();
		CancelInvoke ();
	}

	public void bombPressed() {
		bool soundPlayed = false;
		foreach (GameObject missile in ObjectPoolerScript.current.pooledObjects) {
			if (missile.activeInHierarchy) {
				missile.GetComponent<Animator>().SetBool("isBombPressed", true);
				missile.GetComponent<Rigidbody2D>().velocity = missile.transform.rotation * new Vector2(0, 0);
				GameObject.FindWithTag ("GameController").GetComponent<GameController>().addScore(1);
				if (!soundPlayed) {
					GetComponent<AudioSource>().Play ();
					soundPlayed = true;
				}
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class MissileController : MonoBehaviour {
	public float speed;
	private float timeSincePlop = 0;
	public bool inGracePeriod = true;
	private GameController gameController;
	// Use this for initialization
	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if (gameController == null) {
			Debug.Log("No gameController found");
		}
	}

	void OnEnable() {
		inGracePeriod = true;
		timeSincePlop = 0;
		gameObject.GetComponent<Renderer> ().enabled = true;
		gameObject.GetComponent<Rigidbody2D>().velocity = gameObject.transform.rotation * new Vector2(0, speed);
	}
	
	// Update is called once per frame
	void Update () {
		if (inGracePeriod) {
			timeSincePlop += Time.deltaTime;
			if (timeSincePlop > 3f) {
				inGracePeriod = false;
			}
		} else {
			if (!gameObject.GetComponent<Renderer>().isVisible) {
				gameObject.GetComponent<Renderer>().enabled = false;
				gameObject.SetActive(false);
				if (GameObject.FindWithTag("Cooper").GetComponent<CooperController>().hasGlasses)
				{
					gameController.addScore(2);
				}
				else {
					gameController.addScore(1);
				}
			}
		}
	}

	public void setInactive() {
		gameObject.SetActive (false);
	}
}

﻿using UnityEngine;
using System.Collections;

public class CooperController : MonoBehaviour {
	private bool gameHasStarted;
	public float moveSpeed;
	public bool hasHat;
	public bool hasGlasses;
	public int numberOfBombs;
	private float timeWithItem;
	private Vector3 moveDirection;
	[SerializeField]
	private PolygonCollider2D[] colliders;
	private int currentColliderIndex = 0;
	private Vector3 targetLocation;
	private Vector3 startLocation;
	private float timeSinceStop = 0;
	private float acceleration = .05f;
	private float accelerationsquared = .0f;
	private float lerpTime = 1f;
	private float currentLerpTime;
	AudioSource[] audioSources;
	// Use this for initialization
	void Start () {
		gameHasStarted = false;
		audioSources = GetComponents<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 currentPosition = transform.position;
		if (Input.GetMouseButtonDown(0)) {
			if (!(Input.mousePosition.x > Screen.width - Screen.width / 6+6 && Input.mousePosition.y > Screen.height - Screen.height / 8.5f)) {
				Vector3 moveToward = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				moveDirection = moveToward - currentPosition;
				moveDirection.z = 0;
				moveDirection.Normalize();
				timeSinceStop = 0;
				targetLocation = moveToward;
				startLocation = currentPosition;
				targetLocation.z = 0;
				acceleration = 0;
				accelerationsquared = 0;
				currentLerpTime = 0;
				GetComponent<Animator>().SetBool("isAccelerating", true);
				audioSources[0].Play();
				if (gameHasStarted == false) {
					GameObject.FindWithTag("GameController").GetComponent<GameController>().startGame();
					gameHasStarted = true;
				}

			}
		}
		timeSinceStop += Time.deltaTime;
//		Vector3 target = moveDirection * moveSpeed + currentPosition;
		calculateVelocity ();
		currentLerpTime += Time.deltaTime;
		if (currentLerpTime > lerpTime) {
			currentLerpTime = lerpTime;
		}
		float t = currentLerpTime / lerpTime;
		t = t * t * t * (t * (6f * t - 15f) + 10f);
		transform.position = Vector3.Lerp (startLocation, targetLocation, t);
		if (reached() && moveDirection != Vector3.zero) {
			moveDirection = Vector3.zero;
			GetComponent<Animator>().SetBool("isAccelerating", false);
		}
		facingDirection(moveDirection);
		EnforceBounds ();
		itemTimer ();
	}

	public bool reached() {
		return transform.position.x > targetLocation.x - .2f && transform.position.x < targetLocation.x + .2f && 
			transform.position.y > targetLocation.y - .2f && transform.position.y < targetLocation.y + .2f;
	}

	public void calculateVelocity() {
		Vector3 distanceBetween = targetLocation - startLocation;
		Vector3 currentDistance = targetLocation - transform.position;
		if (distanceBetween.magnitude *.7 < currentDistance.magnitude) {
			acceleration += .03f + accelerationsquared;
		} else {
			acceleration -= .019f;
		}
		accelerationsquared += .0005f;
	}

	public void SetColliderForSprite( int spriteNum )
	{
		colliders[currentColliderIndex].enabled = false;
		currentColliderIndex = spriteNum;
		colliders[currentColliderIndex].enabled = true;
	}

	public void facingDirection(Vector3 moveDirection) {
		if (moveDirection.x > 0) {
			transform.localRotation = Quaternion.Euler(0, 180, 0);
		} else if (moveDirection.x < 0) {
			transform.localRotation = Quaternion.Euler(0, 0, 0);
		}
	}

	private void EnforceBounds()
	{
		Vector3 newPosition = transform.position; 
		Camera mainCamera = Camera.main;
		Vector3 cameraPosition = mainCamera.transform.position;
		
		float xDist = mainCamera.aspect * mainCamera.orthographicSize; 
		float xMax = cameraPosition.x + xDist;
		float xMin = cameraPosition.x - xDist;
		
		if ( newPosition.x < xMin || newPosition.x > xMax ) {
			newPosition.x = Mathf.Clamp( newPosition.x, xMin, xMax );
			moveDirection.x = -moveDirection.x;
		}

		float yMax = mainCamera.orthographicSize;
		if (newPosition.y < -yMax || newPosition.y > yMax) {
			newPosition.y = Mathf.Clamp( newPosition.y, -yMax, yMax );
			moveDirection.y = -moveDirection.y;
		}
		
		transform.position = newPosition;
	}

	void OnTriggerEnter2D( Collider2D other )
	{
		if (other.CompareTag("Missile")) {
			GetComponent<Animator>().SetBool("isHit", true);
			this.enabled = false;
//			Debug.Log ("Hit " + other.gameObject);
			GameObject.FindWithTag ("ObjectFactory").GetComponent<ObjectSpawner>().setAllInactive();
			audioSources[1].Play();
			Invoke("loadGameOverScene", 2.2f);
		}
		if (other.CompareTag ("Hat")) {
			GetComponent<Animator>().SetBool("gotHat", true);
			GetComponent<Animator>().SetBool("gotGlasses", false);
			hasHat = true;
			hasGlasses = false;
			timeWithItem = 0;
			Destroy (other.gameObject);
			audioSources[2].Play();
		}
		if (other.CompareTag ("Glasses")) {
			GetComponent<Animator>().SetBool("gotGlasses", true);
			GetComponent<Animator>().SetBool("gotHat", false);
			hasGlasses = true;
			hasHat = false;
			timeWithItem = 0;
			Destroy (other.gameObject);
			audioSources[2].Play();;
		}
		if (other.CompareTag ("Bomb")) {
			if (numberOfBombs < 3) {
				numberOfBombs++;
			}
			Destroy (other.gameObject);
			audioSources[2].Play();
		}
	}

	void itemTimer() {
		float maximumTime = 7f;
		if (hasHat) {
			timeWithItem += Time.deltaTime;
			if (timeWithItem > maximumTime * 1.4) {
				GetComponent<Animator>().SetBool("gotHat", false);
				hasHat = false;
			}
		}
		else if (hasGlasses) {
			timeWithItem += Time.deltaTime;
			if (timeWithItem > maximumTime) {
				GetComponent<Animator>().SetBool("gotGlasses", false);
				hasGlasses = false;
			}
		}
	}

	void loadGameOverScene() {
		Application.LoadLevel ("GameOverScene");
	}
}
